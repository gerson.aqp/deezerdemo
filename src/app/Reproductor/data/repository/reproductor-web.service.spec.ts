import { TestBed } from '@angular/core/testing';

import { ReproductorWebService } from './reproductor-web.service';

describe('ReproductorWebService', () => {
  let service: ReproductorWebService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReproductorWebService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
