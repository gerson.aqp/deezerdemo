import {Component, OnInit} from "@angular/core";
import {MenuItem} from "primeng/api";
@Component({
	selector: "app-menu",
	templateUrl: "./menu.component.html",
	styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
	opciones: any[];
	constructor() {}

	ngOnInit(): void {
		this.opciones = [
			{
				label: "Mi librería",
				type: "tittle",
				children: [
					{
						label: "Recientes",
						type: "item"
					},
					{
						label: "Artistas",
						type: "item"
					},
					{
						label: "Albums",
						type: "item"
					},
					{
						label: "Canciones",
						type: "item"
					},
					{
						label: "Estaciones",
						type: "item"
					}
				]
			},
			{
				label: "Playlist",
				type: "tittle",
				children: [
					{
						label: "Metal",
						type: "item"
					},
					{
						label: "Para Bailar",
						type: "item"
					},
					{
						label: "Rock 90s",
						type: "item"
					},
					{
						label: "Baladas",
						type: "item"
					}
				]
			}
		];
    }
    action(){
        console.log("algo")
    }
}
