import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MenuComponent} from "./menu/menu.component";
import {ReproductorComponent} from "./reproductor/reproductor.component";
import {BuscadorComponent} from "./buscador/buscador.component";
import {ReproductorRepository} from "../domain/repository/reproductor.repository";
import {ReproductorWebService} from "../data/repository/reproductor-web.service";
import {RecientesComponent} from "./interfaz/recientes/recientes.component";
import {ArtistasComponent} from "./interfaz/artistas/artistas.component";
import {AlbumsComponent} from "./interfaz/albums/albums.component";
import {CancionesComponent} from "./interfaz/canciones/canciones.component";
import {EstacionesComponent} from "./interfaz/estaciones/estaciones.component";
import {PlaylistComponent} from "./interfaz/playlist/playlist.component";
import {InputTextModule} from "primeng/inputtext";

@NgModule({
	imports: [CommonModule, InputTextModule],
	declarations: [
		MenuComponent,
		ReproductorComponent,
		BuscadorComponent,
		RecientesComponent,
		ArtistasComponent,
		AlbumsComponent,
		CancionesComponent,
		EstacionesComponent,
		PlaylistComponent
	],
	exports: [MenuComponent, ReproductorComponent, BuscadorComponent],
	providers: [
		{
			provide: ReproductorRepository,
			useClass: ReproductorWebService
		}
	]
})
export class ReprodInterfaceModule {}
