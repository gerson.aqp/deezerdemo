import {Component, OnInit} from "@angular/core";
import {ReproductorWebService} from "../../data/repository/reproductor-web.service";

@Component({
	selector: "app-buscador",
	templateUrl: "./buscador.component.html",
	styleUrls: ["./buscador.component.scss"]
})
export class BuscadorComponent implements OnInit {
	constructor(public webService: ReproductorWebService) {}

	ngOnInit(): void {}
}
