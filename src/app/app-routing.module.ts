import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecientesComponent } from './Reproductor/presentation/interfaz/recientes/recientes.component';


const routes: Routes = [
    {path: "", component: RecientesComponent,data: {title: "Home"},
    children: [
        {path: "Recientes", component: RecientesComponent, data: {title: "Recientes"}}
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
