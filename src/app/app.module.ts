import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {ReprodInterfaceModule} from "./Reproductor/presentation/reprod-interface.module";
import {ReproductorModule} from "./Reproductor/data/reproductor.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CommonModule } from "@angular/common";
@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		ReprodInterfaceModule,
		ReproductorModule,
		AppRoutingModule,
        FormsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
